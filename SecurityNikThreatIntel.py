#!/usr/bin/env python
# This is code is designed to download list of known bad IPs and domains
# Once the lists have been downloaded, 2 reference sets are created
# 1 for IPs and 1 for domains
# Manual creation of QRadar rules are then done. These rules are then run
# against these list to identify known bad IPs and Domain
#
# SecurityNikThreatIntel.py v1.0
# Authors: Nik Alleyne, CISSP|GCIH|A < nikalleyne@gmail.com >,
# McCrory, James, GWEB|GCIH < james@needtoknowdesigns.com >,
# slick666
# Date Created: 2015-02-25
# Date Updated: 2015-06-08
# Disclaimer: In no way are we responsible for any damages which you may
# cause to your system by running this script.

from os import uname, path, system, remove, getcwd
from shutil import rmtree, copytree
from subprocess import call
from sys import exit
from time import sleep
from ConfigParser import ConfigParser

# set up config object
config = ConfigParser()
config.read('setup.cfg')

# This function checks to see if this script is running on Linux.


def check_os():
    """
    Checks the OS and makes sure it's Linux
    :return:
    """
    qRadar_path = config.get('fileLocations', 'qRadar_path')
    qRadar_ver = config.get('fileLocations', 'qRadar_ver')

    print(' Checking OS ... ')
    if (uname()[0] == 'Linux') or (uname()[0] == 'linux'):
        # print(' Running on Linux ... ')

        if all([path.exists('/etc/system-release'),
                path.isfile('/etc/system-release')]):
            call(['cat', '/etc/system-release'])
        else:
            print('\n Looks like you are running Linux. ')
            print('\n However, I am unable to determine your version info. ')

        print(' \n Looking for an installed version of QRadar')
        if path.exists(qRadar_path) and (path.isdir(qRadar_path)):
            print(' \n looks like you are running QRadar version ... ')
            call([qRadar_ver])
            print(' \n Good stuff ... \n Blast off =>>>>>>> ')
        else:
            print(
                ' An installed version of QRadar was not found on your system ')
            print(
                ' This script will not work for you, it was designed to be used on box running IBM QRadar ')
            print(' Exiting ... ')
            exit(0)

        sleep(2)
    else:
        print(' Running this is a waste of your time. ')
        print(' This script is SPECIFICALLY for QRadar ')
        exit(0)


def grab_list(primary_path, alt_path, bad_list, list_type):
    """
    This function download the list of malicious, and/or suspect paths
    DO NOT add entry to this list unless you are sure what you are doing
    These files are in different formats, thus may need to be manipulated
    the files individually
    :return:
    """
    # Check to see if path exists - This folder stores the files a the first download.
    # Basically this will determine if it's the first time the script is being run
    if path.exists(primary_path) and path.isdir(primary_path):
        primary_path = alt_path
    try:
        print(' Preparing to download list of bad %s' % list_type)
        for link in bad_list:
            print(link.strip())
            call(['wget', link.strip(), '--no-check-certificate', '--directory-prefix=' + primary_path, '--tries=2', '--continue',
			                  '--timestamping', '--timeout=5', '--random-wait', '--no-proxy', '--inet4-only'])
            print(' \n  %s \n retrieved successfully \n' % link)
            sleep(2)
    except:
        print(' A problem occurred while downloading %s information from %s ' % (list_type, link))
        print(' This link may be broken. Please copy the URL and paste into a browser to ensure it is accessible')
    else:
        # Looks like all went well
        print(' \n Looks like we have some baddddd %s!' % list_type)


def compare_dirs(primary_path, alt_path):
    """
    Checking the directories to see if the last run added new info
    :return:
    """
    print(' Checking if there is need for an update .... ')

    # first check to see if path exists
    if path.exists(alt_path) and (path.isdir(alt_path)):
        print(' Give me just a few seconds more')
        sleep(2)

        if int(path.getsize(primary_path)) <= int(path.getsize(alt_path)):
            print(' \n Looks like new content is available ')
            # copying new content in .ip_tmp_path to .ip_tmp
            try:
                rmtree(primary_path)
                copytree(alt_path, primary_path)
            except:
                print(' Failed to copy new data ... ')
                print(' Exiting ... ')
                exit(0)
            else:
                print(' Successfully moved new data')
        else:
            print(' Nothing new was added ... ')
            print(' Exiting ... ')
            exit(0)
    else:
        print(' This is first run ... \n moving on ... ')

    sleep(2)


def combine_files(primary_path, file_name, list_type):
    """
    Now that the files have been successfully downloaded,
    let's combine them all, manipulating as necessary
    :return:
    """
    print(' \n Checking for %s folder ... ' % list_type)
    sleep(2)

    if path.exists(primary_path) and path.isdir(primary_path):
        print(' directory %s found ' % primary_path)
        if list_type == 'IP':
            system(
                'cat %s* | '
                'grep --perl-regexp --only-matching "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" | '
                'sort -i | '
                'uniq --unique --check-chars=15 > %s' %
                (primary_path, file_name)
            )
            if path.exists(file_name) and path.isfile(file_name):
                print(' Successfully created file %s ' % file_name)
            else:
                print(' Unable to create %s file ' % file_name)
                print(' The program will now exit ... Exiting ... ')
                exit(0)

        elif list_type == 'DNS':
            try:
                print(' Combining downloaded files into .... ')
                # here is where we need to do some testing of the files
                system('cat %s/dom-bl.txt > .%s' % (primary_path, file_name))
                system('cat %s/dom-bl-base.txt >> .%s' %
                       (primary_path, file_name))
                system("cat %s/hosts.txt | awk  '/127.0.0.1/ { print $2 }'  >> .%s" % (primary_path, file_name))
                system('cat %s/immortal_domains.txt | grep -i -P "This is a list|^$" -v >> .%s' %
                       (primary_path, file_name))
                system('cat %s/BOOT | grep -i PRIMARY | cut -f 2 -d " " | grep -i -v -P "ibm\.com" -v >> .%s' %
                       (primary_path, file_name))
                system('cat %s/dynamic_dns.txt | grep -P -v "^#|^$" | cut -f 1 -s >> .%s' %
                       (primary_path, file_name))
                system('cat %s/blocklist.php\?download\=baddomains | grep -P -v "^#|^$" >> .%s' %
                       (primary_path, file_name))
                system('cat .%s | sort -i | uniq --unique > %s' %
                       (file_name, file_name))

            except:
                print(' Looks like an error occurred while combining the files')
                print(' Please retry later ... \n Exiting ... ')
                exit(0)
            else:
                print(' files successfully combined ')
                print(' A list of known bad domains can be found in %s' %
                      file_name)
                remove('.%s' % file_name)
        else:
            print(' Script is not yet setup for this type of list.')

    else:
        print(' \n %s directory not found ' % primary_path)
        print(' Unable to continue ... Exiting!')
        exit(0)


def verify_create_reference_set(file_name, reference_set_name, count_file, list_type):
    """
    This function does all the work for the IP reference set
    :return:
    """
    txt = getcwd() + '/%s' % file_name
    rows = []

    print('Checking to see if the reference set %s already exists' %
          reference_set_name)
    f = open('.%s' % count_file, 'w')
    call(["psql", "-U", "qradar", "--command=SELECT COUNT(*) FROM reference_data WHERE name='%s'" %
          reference_set_name], stdout=f)
    f.close()

    # Resting ... I'm tired
    sleep(2)

    f = open('.%s' % count_file, 'r')

    for line in f.readlines():
        rows.append(line.strip())

    if rows[2].strip() != '0':
        print(' Looks like reference set already exists \n ')
    else:
        print(' Reference Set %s not found ' % reference_set_name)
        print(' Looks like we will have to create this bad boy ...')

        try:
            call(
                ['/opt/qradar/bin/ReferenceSetUtil.sh', 'create', reference_set_name,
                 list_type])
            print(' Successfully created reference set %s \n ' %
                  reference_set_name)

        except:
            # This does not catch any java exception that may be created
            print(' Error occurred while creating reference set %s ' %
                  reference_set_name)
            print(' You may create the reference set %s manually if needed ' %
                  reference_set_name)
            exit(0)

    print(' Loading information into reference set %s ' % reference_set_name)

    try:
        call(
            ['/opt/qradar/bin/ReferenceSetUtil.sh', 'load', reference_set_name, txt])
        print(' \n You may need to verify that you have rules created to use %s ' %
              reference_set_name)
    except:
        print(' An error occurred while loading the reference set ... ')
        print(' Please retry later!')
        exit(0)
    remove('.%s' % count_file)


# Main Function
def main():
    call('clear')
    check_os()

    # Let's work on the IP Reference Set
    grab_list(config.get('fileLocations', 'ip_first_tmp_path'),
              config.get('fileLocations', 'ip_other_tmp_path'),
              config.get('jsonListURLs', 'IPs').split(","),
              "IPs")
    compare_dirs(config.get('fileLocations', 'ip_first_tmp_path'),
                 config.get('fileLocations', 'ip_other_tmp_path'))
    combine_files(config.get('fileLocations', 'ip_first_tmp_path'),
                  config.get('fileLocations', 'ip_storage_file'),
                  'IP')
    verify_create_reference_set(config.get('fileLocations', 'ip_storage_file'),
                                config.get('fileLocations', 'reference_set_name'),
                                config.get('fileLocations', 'count_file'),
                                'IP')

    # Let's work on the DNS Reference Set
    grab_list(config.get('fileLocations', 'dns_first_tmp_path'),
              config.get('fileLocations', 'dns_other_tmp_path'),
              config.get('jsonListURLs', 'domains').split(","),
              "domains")
    compare_dirs(config.get('fileLocations', 'dns_first_tmp_path'),
                 config.get('fileLocations', 'dns_other_tmp_path'))
    combine_files(config.get('fileLocations', 'dns_first_tmp_path'),
                  config.get('fileLocations', 'dns_storage_file'),
                  'DNS')
    verify_create_reference_set(config.get('fileLocations', 'dns_storage_file'),
                                config.get('fileLocations', 'reference_dns_set_name'),
                                config.get('fileLocations', 'count_file'),
                                'ALN')

    # Let's work on the URL reference Set
    grab_list(config.get('fileLocations', 'url_first_tmp_path'),
              config.get('fileLocations', 'url_other_tmp_path'),
              config.get('jsonListURLs', 'URLs').split(","),
              "URLs")


if __name__ == "__main__":
    main()
